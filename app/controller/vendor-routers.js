import Router from "koa-router";
import VendorService from "../service/vendor-service";

let router = new Router();

router.get("/vendor/getVendors", async (ctx, next) => {
  let vendors=new VendorService().getVendors();
  ctx.body = JSON.stringify(vendors);
});

export default router;
