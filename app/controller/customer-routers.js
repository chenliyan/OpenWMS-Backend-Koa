import Router from "koa-router";
import customerService from "../service/customer-service";
import koaBody from "koa-body";
let router = new Router({
  prefix: "customers"
});

router.get("/:id", async (ctx, next) => {
  const warehouse = customerService.getById(ctx.params.id);
  ctx.body = warehouse;
  next();
});

router.get("/", async (ctx, next) => {
  let customers = customerService.list();
  ctx.body = customers;
  next();
});

router.post("/", koaBody(), async (ctx, next) => {
  const res = customerService.add(ctx.request.body);
  ctx.body = res;
  next();
});

router.put("/:id", koaBody(), async (ctx, next) => {
  const id = ctx.params.id;
  const res = customerService.update(id, ctx.request.body);
  ctx.body = res;
  next();
});

router.delete("/:id", async (ctx, next) => {
  const id = ctx.params.id;
  const res = customerService.delete(id);
  ctx.body = res;
  next();
});

export default router;
