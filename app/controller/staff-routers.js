import Router from "koa-router";
import staffService from "../service/staff-service";
import koaBody from "koa-body";
const router = new Router({prefix: "/staffs"});

router.get("/:id", async (ctx, next) => {
  const staff = await staffService.getById(ctx.params.id);
  ctx.body = staff;
  next();
});

router.get("/", async (ctx, next) => {
  const staffs = await staffService.list(ctx.query);
  ctx.body = staffs;
  next();
});

router.post("/", koaBody(), async (ctx, next) => {
  const res = staffService.add(ctx.request.body);
  ctx.body = res;
  next();
});

router.put("/:id", koaBody(), async (ctx, next) => {
  const id = ctx.params.id;
  const res = staffService.update(id, ctx.request.body);
  ctx.body = res;
  next();
});

router.delete("/:id", async (ctx, next) => {
  const id = ctx.params.id;
  const res = staffService.delete(id);
  ctx.body = res;
  next();
});

export default router;
