import Router from "koa-router";
import InboundService from "../service/inbound-service";

let router = new Router();

router.get("/inbound/getInboundDetails", async (ctx, next) => {
  let inboundDetails=new InboundService().getInboundDetails();
  ctx.body = JSON.stringify(inboundDetails);
});

router.get("/inbound/getInboundReceipt", async (ctx, next) => {    
  let inboundReceipt=new InboundService().getInboundReceipt();
  ctx.body = JSON.stringify(inboundReceipt);
});

router.get("/inbound/getInboundRecords", async (ctx, next) => {    
  let inboundRecords=new InboundService().getInboundRecords();
  ctx.body = JSON.stringify(inboundRecords);
});

export default router;
