import Router from "koa-router";
import InventoryService from "../service/inventory-service";

let router = new Router();

router.get("/inventory/getInventories", async (ctx, next) => {
  let inventories=new InventoryService().getInventories();
  ctx.body = JSON.stringify(inventories);
});

export default router;
