import dbHelper from '../utils/better-db-helper';
import BaseService from './BaseService';

class CategoryService extends BaseService{
  emptyData = {
    index: "",
    name: "",
    creator:"",
    createTime: "",
    remark: ""
  }
  getById(id) {
    return dbHelper.getById(`select * from category where id=@id`, {id});
  }
  list(params) {
    const {pageNumber, pageSize, index, name} = Object.assign({}, this.emptyData, this.pagination, params);
    const whereSegment = `where index like @index and name like @name`;

    const sqlParams = {
      index: `%${index}%`,
      name: `%${name}%`
    };

    //total
    const total = dbHelper.count(`select count(*) from category ${whereSegment}`, sqlParams);
    //rows
    const rows = dbHelper.queryRows(`select 
      id, index,
      name,creator,
      create_time createTime,
      remark
      from user ${whereSegment} limit @offset, @limit`,
    Object.assign(sqlParams, {
      offset: (pageNumber - 1) * pageSize,
      limit: pageSize
    }));
    return {
      rows,
      total
    };
  }

  add(params) {
    return dbHelper.insert("insert into category values(null, @index, @name, @creator,@createTime, @remark)", Object.assign({}, this.emptyData, params));
  }

  update(id, params) {
    return dbHelper.update("update category set index=@index, name=@name, remark=@remark where id =@id", Object.assign({
      id
    }, this.emptyData, params));
  }

  delete(id) {
    return dbHelper.delete("delete from category where id = @id", {
      id
    });
  }
}

export default new CategoryService;
