/**
 * log4js, global singleton
 */
import log4js from "log4js";

log4js.configure({
  appenders: {
    openwms: {
      type: "console", //type=file会导致babel-watch无法重启
      filename: "./log/openwms.log",
      maxLogSize: 20 * 1024 * 1024, //byte
      compress: true,
      layout: {
        type: "pattern",
        pattern: "%d{yyyy-MM-dd hh:mm:ss} %p %c %m"
      }
    }
  },
  categories: {
    default: {
      appenders: ["openwms"],
      level: "trace"
    }
  }
});
let logger = log4js.getLogger();

export default logger;
