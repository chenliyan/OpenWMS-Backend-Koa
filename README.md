## OpenWMS-Backend-Koa

### 项目介绍

基于 Koa 的服务端。

### 用法

    git clone https://gitee.com/mumu-osc/OpenWMS-Backend-Koa.git
    cd OpenWMS-Backend-Koa
    cnpm install
    cnpm start

### 使用Vscode调试
在你的launch.json配置文件中添加
```
{
      "type": "node",
      "request": "launch",
      "name": "Launch Nodemon Program",
      "runtimeExecutable": "nodemon",
      "program": "${workspaceFolder}/app/app.js",
      "restart": true,
      "console":"integratedTerminal",
      "internalConsoleOptions":"neverOpen",
      "runtimeArgs": [
        "--watch",
        "app",
        "--delay",
        "2.5"
      ],
      "outFiles": [
        "${workspaceFolder}/dist/**/*.js"
      ]
    }
```

然后在命令行运行
```
npm run compile
```
然后点击vscode的debug菜单，选择开始调试。


打开你的浏览器访问http://localhost:3000
